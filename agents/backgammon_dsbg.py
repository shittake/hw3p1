'''
Name: Jin Terada White
UW netid(s): jintw
'''

from game_engine import genmoves
from game_engine import boardState
import math 

class BackgammonPlayer:
    def __init__(self):
        self.GenMoveInstance = genmoves.GenMoves()
        self.states = 0
        self.prunes = 0
        self.will_prune = True
        self.max_ply = -1
        self.static_eval = self.staticEval

    # returns a string representing a unique nick name for your agent
    def nickname(self):
        # TODO: return a string representation of your UW netid(s)
        return "jintw"

    # If prune==True, changes the search algorthm from minimax
    # to Alpha-Beta Pruning
    #
    # Should reset both the counters for states created and for Alpha-Beta cutoffs to 0.
    # every time theres a prune increment the counter
    def useAlphaBetaPruning(self, prune=False):
        self.states = 0
        self.prunes = 0
        self.will_prune = prune
        # TODO: use the prune flag to indiciate that your search
        # should use Alpha-Beta rather than minimax

    # Returns a tuple containing the number explored
    # states as well as the number of cutoffs.
    def statesAndCutoffsCounts(self):
        # TODO: return a tuple containig states and cutoff
        return (self.states, self.prunes)

    # Given a ply, it sets a maximum for how far an agent
    # should go down in the search tree. If maxply==-1,
    # no limit is set
    def setMaxPly(self, maxply=-1):
        self.max_ply = maxply

    # If not None, it updates the internal static evaluation
    # function to be func
    def useSpecialStaticEval(self, func):
        self.static_eval = func if self.static_eval != None else self.staticEval
    
    # Given a state and a roll of dice, it returns the best move for
    # the state.whose_move

    # 1. move
    # 2. minimax on the state
    # 3. recursively go down and find the max static eval
    def move(self, state, die1, die2):
        """The is the primary method of this class. It returns a move."""
        # find all moves we can do in the current state
        # perform the minimax on each one
        # the one with the highest value is the move we return
        move_generator = self.GenMoveInstance.gen_moves(state, state.whose_move, die1, die2)
        primary_moves = self.get_all_possible_moves(move_generator)
        # print("Moves to choose from:", [moves[0] for moves in primary_moves])
        maxEval = -math.inf
        maxMove = primary_moves[0]
        if primary_moves == ['p']:
            return maxMove
        depth = self.max_ply-1 if self.max_ply != -1 else 3
        for move in primary_moves:
            # print("minimax on move:", move[0])
            eval = self.move_minimax(move[1], depth, -math.inf, math.inf, False)
            # print("eval of", eval, "for move", move[0])
            if eval > maxEval:
                maxEval = eval
                maxMove = move[0]
        # print("best move was", maxMove)
        return maxMove
    # DRAW TREE OF WHATS GOING ON
    def move_minimax(self, state, depth, alpha, beta, maximizingPlayer):
        # print("THE BAR:", state.bar)
        # print("Depth of:", depth)
        self.states += 1
        # print("states explored:", self.states)
        # print("Checking if depth is reached or game over")
        # print("Red Off:",len(state.red_off))
        # print("White Off", len(state.white_off))
        if (depth != -1 and depth == 0) or len(state.red_off) >= 15 or len(state.white_off) >= 15:
            return self.static_eval(state)
        #change so that we generate all possible moves on the go rather than generated ones
        move_generator = self.GenMoveInstance.gen_moves(state, state.whose_move, 1, 6)
        # all possible (move,state) pairs of our state
        moves = self.get_all_possible_moves(move_generator)
        # print("Moves to choose from:", [move[0] for move in moves])
        if maximizingPlayer:
            # print("Maximizing player:")
            maxEval = -math.inf
            for move in moves:
                # print("Looking at move", move[0])
                if move == 'p': 
                    switch_state = boardState.bgstate(state)
                    switch_state.whose_move = 0 if switch_state.whose_move == 1 else 1
                    eval = self.move_minimax(switch_state, depth-1, alpha, beta, False)
                else:
                    eval = self.move_minimax(move[1], depth-1, alpha, beta, False)
                maxEval = max(maxEval, eval)

                # prune or not
                if self.will_prune:
                    alpha = max(alpha, eval)
                    if (beta <= alpha):
                        self.prunes += 1
                        break
            # return the move if we are at the top depth
            return maxEval
        else:
            # print("Minimizing player:")
            minEval = math.inf
            for move in moves:
                # print("Move iteration:")
                # print("m:", move)
                # print("Looking at move", move[0])
                if move == 'p':
                    switch_state = boardState.bgstate(state)
                    switch_state.whose_move = 0 if switch_state.whose_move == 1 else 1
                    eval = self.move_minimax(switch_state, depth-1, alpha, beta, True)
                else:
                    eval = self.move_minimax(move[1], depth-1, alpha, beta, True)
                minEval = min(minEval, eval)
                # prune or not
                if self.will_prune:
                    beta = min(beta, eval)
                    if beta <= alpha:
                        self.prunes += 1
                        break
            return minEval

    def get_all_possible_moves(self, move_generator):
        """Uses the mover to generate all legal moves. Returns an array of move commands"""
        move_list = []
        done_finding_moves = False
        any_non_pass_moves = False
        while not done_finding_moves:
            try:
                m = next(move_generator)    # Gets a (move, state) pair.
                # print("next returns: ",m[0]) # Prints out the move.    For debugging.
                if m[0] != 'p':
                    any_non_pass_moves = True
                    move_list.append(m)    # Add the move to the list.
            except StopIteration as e:
                done_finding_moves = True
        if not any_non_pass_moves:
            move_list.append('p')
        # print("movelist:", move_list)
        return move_list

    # Given a state, returns an integer which represents how good the state is
    # for the two players (W and R) -- more positive numbers are good for W
    # while more negative numbers are good for R
    def staticEval(self, state):
        #  taking off, barring a piece, and general advancements
        # TODO: return a number for the given state
        white_score = 0
        red_score = 0

        # add a "it's bad if we cant bear off"
        #white to bear off
        white_score += len(state.white_off) * 10
        #red to bear off
        red_score += len(state.red_off) * 10

        # evaluating checker positions in Red's home board
        for checkers in range(0,6):
            if len(state.pointLists[checkers]) > 0:
                if 'W' in state.pointLists[checkers]:
                    white_score -= 3 * len(state.pointLists[checkers])
                else:
                    red_score += 3 * len(state.pointLists[checkers])
        
        # evaluating checker positions in Red's outer board
        for checkers in range(6,12):
            if len(state.pointLists[checkers]) > 0:
                if 'R' in state.pointLists[checkers]:
                    red_score -= len(state.pointLists[checkers])
                else:
                    white_score -= 2 * len(state.pointLists[checkers])

        # evaluating checker positions in White's outer board   
        for checkers in range(12,18):
            if len(state.pointLists[checkers]) > 0:
                if 'W' in state.pointLists[checkers]:
                    white_score -= len(state.pointLists[checkers])
                else:
                    red_score -= 2*len(state.pointLists[checkers])

        # evaluating checker positions in White's home board
        for checkers in range(18,24):
            if len(state.pointLists[checkers]) > 0:
                if 'W' in state.pointLists[checkers]:
                    white_score += 3 * len(state.pointLists[checkers])
                else:
                    red_score -= 3 *len(state.pointLists[checkers])
        
        for bar in state.bar:
            # white hit to bar
            if bar == 0:
                white_score += 20
            # red hit to bar
            elif bar == 1:
                red_score -= 30
        
        if len(state.white_off) > 13:
            white_score += 500
        elif len(state.white_off) > 14:
            white_score += 10000
        
        if len(state.red_off) > 13:
            red_score += 500
        elif len(state.red_off) > 14:
            red_score += 10000


        # for point in state.pointLists:
        #     if len(point) == 1:
        #         if point[0] == 'W':
        #             white_score -= 5
        #         else:
        #             red_score -= 5
        print("static eval:", white_score-red_score)
        return white_score - red_score